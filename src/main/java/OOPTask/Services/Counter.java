package OOPTask.Services;

import OOPTask.Models.PassengerTrain;

import java.util.List;

public interface Counter {
    public List<PassengerTrain> service(List<PassengerTrain> passengerTrains);
}
