package OOPTask;

import OOPTask.Templates.Actions;
import OOPTask.Templates.LevelComfort;
import OOPTask.Models.PassengerTrain;
import OOPTask.Models.Wagon;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        List<PassengerTrain> passengerTrains = new ArrayList<>();
        passengerTrains.add(new PassengerTrain("Lviv-Kyiv", "Andy",
                new Wagon(52,200,LevelComfort.BUSINESS)));
        passengerTrains.add(new PassengerTrain("Lviv-Rivne", "Borys",
                new Wagon(45,100,LevelComfort.LUXURY)));
        passengerTrains.add(new PassengerTrain("London-Warsaw", "Sasha",
                new Wagon(60,250,LevelComfort.PREMIUM)));
        passengerTrains.add(new PassengerTrain("LA-Washington", "John",
                new Wagon(30,50,LevelComfort.LUXURY)));
        String menu = "Please, choose the action:\n" +
                "1)Count number of passengers\n" +
                "2)Count value of cargo\n" +
                "3)Sort by comfort level\n" +
                "4)Find train by number of passengers\n" +
                "5)Quit";
        System.out.println(menu);
        Scanner sc = new Scanner(System.in);
        int choose = sc.nextInt();
        while (choose > 0 && choose < 5) {
            switch (choose) {
                case 1:
                    List<PassengerTrain> passengerTrainsForPassengerCounting = Actions.COUNT_NUMBER_OF_PASSENGERS.service(passengerTrains);
                    int numOfPassengers = 0;
                    for (PassengerTrain passengerTrain : passengerTrainsForPassengerCounting) {
                        numOfPassengers += passengerTrain.getWagon().getNumPassengers();
                    }
                    System.out.println("There are " + numOfPassengers + " passengers in the train");
                    break;
                case 2:
                    List<PassengerTrain> passengerTrainsForCargoValue = Actions.COUNT_VALUE_OF_CARGO.service(passengerTrains);
                    int cargoValue = 0;
                    for (PassengerTrain passengerTrain : passengerTrainsForCargoValue) {
                        cargoValue += passengerTrain.getWagon().getValueCargo();
                    }
                    System.out.println("There are " + cargoValue + " total value of cargo in the train");
                    break;
                case 3:
                    Actions.SORT_BY_COMFORT_LEVEL.service(passengerTrains);
                    System.out.println(passengerTrains);
                    break;
                case 4:
                    Actions.FILTER_BY_NUMBER_OF_PASSENGERS.service(passengerTrains);
                    System.out.println("Type minimal number of passengers");
                    int rangeFrom = sc.nextInt();
                    System.out.println("Type maximum number of passengers");
                    int rangeTo = sc.nextInt();
                    List<PassengerTrain> trains = passengerTrains.stream().filter(passengerTrain -> passengerTrain.getWagon().getNumPassengers() > rangeFrom
                    &&
                    passengerTrain.getWagon().getNumPassengers() < rangeTo)
                    .collect(Collectors.toList());
                    System.out.println(trains);
                    break;
                case 5:
                    System.out.println("Good bye!!!");
                    choose = 0;
                    break;
            }
            System.out.println(menu);
            choose = sc.nextInt();
        }


    }
}
