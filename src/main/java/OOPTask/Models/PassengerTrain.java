package OOPTask.Models;

import java.util.Objects;

public class PassengerTrain extends Train {
    private String conductor;
    private Wagon wagon;

    public PassengerTrain(String connection, String conductor, Wagon wagon) {
        super(connection);
        this.conductor = conductor;
        this.wagon = wagon;
    }

    public String getConductor() {
        return conductor;
    }

    public void setConductor(String conductor) {
        this.conductor = conductor;
    }

    public Wagon getWagon() {
        return wagon;
    }

    public void setWagon(Wagon wagon) {
        this.wagon = wagon;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        PassengerTrain that = (PassengerTrain) o;
        return Objects.equals(conductor, that.conductor) &&
                Objects.equals(wagon, that.wagon);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), conductor, wagon);
    }

    @Override
    public String toString() {
        return "PassengerTrain{" +
                "conductor='" + conductor + '\'' +
                ", wagon=" + wagon +
                '}';
    }
}
