package OOPTask.Models;

import OOPTask.Templates.LevelComfort;

import java.util.Objects;

public class Wagon {
    private int numPassengers;
    private int valueCargo;
    private LevelComfort levelComfort;

    public Wagon(int numPassengers, int valueCargo, LevelComfort levelComfort) {
        this.numPassengers = numPassengers;
        this.valueCargo = valueCargo;
        this.levelComfort = levelComfort;
    }

    public int getNumPassengers() {
        return numPassengers;
    }

    public void setNumPassengers(int numPassengers) {
        this.numPassengers = numPassengers;
    }

    public int getValueCargo() {
        return valueCargo;
    }

    public void setValueCargo(int valueCargo) {
        this.valueCargo = valueCargo;
    }

    public LevelComfort getLevelComfort() {
        return levelComfort;
    }

    public void setLevelComfort(LevelComfort levelComfort) {
        this.levelComfort = levelComfort;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Wagon wagon = (Wagon) o;
        return numPassengers == wagon.numPassengers &&
                valueCargo == wagon.valueCargo &&
                levelComfort == wagon.levelComfort;
    }

    @Override
    public int hashCode() {
        return Objects.hash(numPassengers, valueCargo, levelComfort);
    }

    @Override
    public String toString() {
        return "Wagon{" +
                "numPassengers=" + numPassengers +
                ", valueCargo=" + valueCargo +
                ", levelComfort=" + levelComfort +
                '}';
    }
}
