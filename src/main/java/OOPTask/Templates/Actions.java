package OOPTask.Templates;

import OOPTask.Models.PassengerTrain;
import OOPTask.Services.Counter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


public enum Actions implements Counter {
    COUNT_NUMBER_OF_PASSENGERS{
        @Override
        public List<PassengerTrain> service(List<PassengerTrain> passengerTrains) {
            return passengerTrains;
        }
    },
    COUNT_VALUE_OF_CARGO {
        @Override
        public List<PassengerTrain> service(List<PassengerTrain> passengerTrains) {
            return passengerTrains;
        }
    },
    SORT_BY_COMFORT_LEVEL {
        @Override
        public List<PassengerTrain> service(List<PassengerTrain> passengerTrains) {
            Collections.sort(passengerTrains,
                    (Comparator.comparingInt(o -> o.getWagon()
                            .getLevelComfort()
                            .ordinal())));

            return passengerTrains;
        }
    },
    FILTER_BY_NUMBER_OF_PASSENGERS{
        @Override
        public List<PassengerTrain> service(List<PassengerTrain> passengerTrains) {

            return passengerTrains;
        }
    }
}




