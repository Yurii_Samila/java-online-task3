package ExceptionsTask;

public class MyAutoCloseable implements AutoCloseable{

    @Override
    public void close() throws Exception {
        System.out.println("AutoCloseble works!");
    }
}
